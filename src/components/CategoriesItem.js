import React from "react"
import { Text, StyleSheet, View, Image, TouchableOpacity } from "react-native";

export default function CategoriesItem(props) {
  return (
    <TouchableOpacity activeOpacity={0.5}
      onPress={props.onPress}
    >
      <View style={styles.container}>
        <Image style={styles.img}
          source={require("../assets/images/cs1.png")}
        />
        <View style={styles.rightContainer}>
          <Text style={styles.title}>{props.category.name}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: 10,
    borderRadius: 4,
    alignItems: "stretch",
    backgroundColor: "#E97474",
    shadowColor: "#000",
    shadowOpacity: 0.8,
    shadowRadius: 10,
    shadowOffset: { width: 0, height: 0 },
    elevation: 1,
    flexDirection: "row"
  },
  title: {
    textTransform: "uppercase",
    fontWeight: "bold",
  },
  img: {
    width: 100,
    height: 100,
  },
  rightContainer: {
    flex: 1,
    justifyContent: "center",
    paddingLeft: 16,
  }
});
