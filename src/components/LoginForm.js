import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {useState} from 'react';
import {
  Button,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import axios from 'react-native-axios';
import Card from './Card';

export default function LoginForm({navigation, setIsLoggedIn}) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const logIn = () => {
    axios
      .post(`${axios.defaults.baseUrl}/users/login`, null, {
        auth: {
          username: username,
          password: password,
        },
      })
      .then(res => {
        const storeToken = async () => {
          try {
            const token = res.data.token;
            await AsyncStorage.setItem('token', token);
            axios.defaults.headers['Authorization'] = `Bearer ${token}`;
          } catch (e) {
            console.error(err);
          }
        };
        storeToken();
        setIsLoggedIn(true);
      })
      .catch(err => console.error(err));
  };

  return (
    <View style={styles.screenCenter}>
      <Card style={styles.authContainer}>
        <ScrollView>
          <Text style={styles.label}>Username</Text>
          <TextInput
            style={styles.input}
            value={username}
            onChangeText={newText => setUsername(newText)}
          />
          <Text style={styles.label}>Password</Text>
          <TextInput
            style={styles.input}
            secureTextEntry
            value={password}
            onChangeText={newText => setPassword(newText)}
          />
          <View style={styles.buttonContainer}>
            <Button title="Login" color="red" onPress={logIn} />
          </View>
          <View style={styles.buttonContainer}>
            <Button
              title="Switch to Sign Up"
              color="gray"
              onPress={() => navigation.navigate('SignUp')}
            />
          </View>
        </ScrollView>
      </Card>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
  screenCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  authContainer: {
    width: '80%',
    maxWidth: 400,
    maxHeight: 400,
    padding: 20,
  },
  buttonContainer: {
    marginTop: 10,
  },
  input: {
    paddingHorizontal: 2,
    paddingVertical: 5,
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
  },
  label: {
    fontFamily: 'open-sans-bold',
    marginVertical: 8,
  },
});
