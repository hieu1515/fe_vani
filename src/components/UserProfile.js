import React, {useState, useEffect} from 'react';
import {Text, View, StyleSheet, Button} from 'react-native';
import axios from 'react-native-axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function UserProfile({setIsLoggedIn}) {
  const [userInfo, setUserInfo] = useState({
    username: '',
    name: '',
    email: '',
    address: '',
    phonenumber: '',
  });

  useEffect(() => {
    const fetchUserInfo = async () => {
      try {
        axios
          .get(`${axios.defaults.baseUrl}/users/myinfo`)
          .then(res => {
            setUserInfo(res.data[0]);
          })
          .catch(err => console.error(err));
      } catch (e) {
        alert('Error reading string!');
      }
    };

    fetchUserInfo();
  }, []);

  return (
    <View style={styles.screenCenter}>
      <Button
        title="Logout"
        color="red"
        onPress={() => {
          const removeToken = async () => {
            try {
              await AsyncStorage.removeItem('token');
              delete axios.defaults.headers["Authorization"];
            } catch (e) {
              // remove error
            }
          };
          removeToken();
          setIsLoggedIn(false);
        }}
      />
      <Text style={styles.label}>Full Name:</Text>
      <Text style={styles.userInfo}>{userInfo.name}</Text>

      <Text style={styles.label}>Email: </Text>
      <Text style={styles.userInfo}>{userInfo.email}</Text>

      <Text style={styles.label}>Address: </Text>
      <Text style={styles.userInfo}>{userInfo.address}</Text>

      <Text style={styles.label}>Phone Number: </Text>
      <Text style={styles.userInfo}>{userInfo.phonenumber}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
  screenCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  authContainer: {
    width: '80%',
    maxWidth: 400,
    maxHeight: 400,
    padding: 20,
  },
  buttonContainer: {
    marginTop: 10,
  },
  input: {
    paddingHorizontal: 2,
    paddingVertical: 5,
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
  },
  label: {
    fontFamily: 'open-sans-bold',
    marginVertical: 8,
    fontWeight: '900',
    fontSize: 20,
  },
  userInfo: {
    fontFamily: 'open-sans-bold',
    marginVertical: 8,
    fontSize: 20,
  },
});
