import React from "react"
import { Text, StyleSheet, View, Image, TouchableOpacity } from "react-native";
import axios from "react-native-axios";

export default function ProductsItem(props) {
  // const linkImg = "https://static.wikia.nocookie.net/nana/images/d/d4/Nana-Osaki.jpg/revision/latest/top-crop/width/360/height/450?cb=20180107024928"
  const linkImg = `${axios.defaults.baseUrl}/images/${props.product.image}`
  return (
    <TouchableOpacity activeOpacity={0.5} onPress={props.onPress}>
      <View style={styles.container} flexDirection="column">
        <Image style={styles.img}
          source={{uri: linkImg}}
        />
        <Text style={[styles.text, styles.name]}>{props.product.name}</Text>
        <Text style={[styles.text, styles.price]}>{props.product.price} $</Text>
        {/* <Text style={styles.rating}>{props.product.rating}</Text> */}
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 4,
    // alignItems: "center",
    // backgroundColor: "#E97474",
    backgroundColor: "white",
  },
  img: {
    width: "100%",
    aspectRatio: 1,
    alignItems: "center",
  },
  text: {
    color: "black",
  },
  name: {
    fontWeight: "bold",
    padding: 10,
    paddingBottom: 0,
  },
  price: {
    padding: 4,
    paddingLeft: 10,
    color: "red",
  },
});
