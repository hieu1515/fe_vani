import React, {useState} from 'react';
import {Button, ScrollView, StyleSheet, View, Text, TextInput} from 'react-native';
import Card from './Card';
import axios from 'react-native-axios';

const SignUp = ({navigation}) => {
  const [userInfo, setUserInfo] = useState({
    username: '',
    password: '',
    name: '',
    address: '',
    email: '',
    phonenumber: '',
    account_type: 2,
  });
  const _setUserInfo = obj => setUserInfo({...userInfo, ...obj});

  const signUp = () => {
    axios
      .post(`${axios.defaults.baseUrl}/users/register`, userInfo)
      .then(res => {
        if (res.status === 200){
          alert("Username or email exists. Please choose another one")
        } else if (res.status === 201) {
          alert("Signing up success.")
          navigation.navigate('User')
        }
      })
      .catch(err => console.error(err));
  };

  return (
    <View style={styles.screenCenter}>
      <Card style={styles.authContainer}>
        <ScrollView>
          <Text style={styles.label}>Username</Text>
          <TextInput
            style={styles.input}
            value={userInfo.username}
            onChangeText={newText => _setUserInfo({username: newText})}
          />
          <Text style={styles.label}>Password</Text>
          <TextInput
            style={styles.input}
            secureTextEntry
            value={userInfo.password}
            onChangeText={newText => _setUserInfo({password: newText})}
          />
          <Text style={styles.label}>Full Name</Text>
          <TextInput
            style={styles.input}
            value={userInfo.name}
            onChangeText={newText => _setUserInfo({name: newText})}
          />
          <Text style={styles.label}>Address</Text>
          <TextInput
            style={styles.input}
            value={userInfo.address}
            onChangeText={newText => _setUserInfo({address: newText})}
          />
          <Text style={styles.label}>Email</Text>
          <TextInput
            style={styles.input}
            value={userInfo.email}
            onChangeText={newText => _setUserInfo({email: newText})}
            keyboardType="email-address"
          />
          <Text style={styles.label}>Phone Number</Text>
          <TextInput
            style={styles.input}
            value={userInfo.phonenumber}
            onChangeText={newText => _setUserInfo({phonenumber: newText})}
            keyboardType="phone-pad"
          />
          <View style={styles.buttonContainer}>
            <Button title="Sign Up" color="red" onPress={signUp} />
          </View>
          <View style={styles.buttonContainer}>
          </View>
        </ScrollView>
      </Card>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
  screenCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  authContainer: {
    width: '80%',
    maxWidth: 400,
    maxHeight: 400,
    padding: 20,
  },
  buttonContainer: {
    marginTop: 10,
  },
  input: {
    paddingHorizontal: 2,
    paddingVertical: 5,
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
  },
  label: {
    fontFamily: 'open-sans-bold',
    marginVertical: 8,
  },
});

export default SignUp;
