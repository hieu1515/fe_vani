import React from 'react'
import { View, StyleSheet } from 'react-native'
import { Ionicons } from '@expo/vector-icons';
import { FontAwesome5 } from '@expo/vector-icons';

const Footer = () => {
  return (
    <View style={styles.container}>
      <Ionicons name="home-outline" size={24} color="orange"></Ionicons>
      <FontAwesome5 name="list" size={24} color="orange" />
      <Ionicons name="notifications-outline" size={24} color="orange"></Ionicons>
      <Ionicons name="cart-outline" size={24} color="orange"></Ionicons>
      <FontAwesome5 name="user" size={24} color="orange" />
    </View>
  )
}

export default Footer

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: "space-evenly",
    marginVertical: 8,
  },
});
