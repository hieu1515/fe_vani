import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {useEffect, useState} from 'react';
import {StyleSheet} from 'react-native';
import LoginForm from '../components/LoginForm';
import UserProfile from '../components/UserProfile';

const User = ({navigation}) => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    const checkToken = async () => {
      try {
        const token = await AsyncStorage.getItem('token');
        if (token) {
          setIsLoggedIn(true);
        }
      } catch (e) {
        // read error
      }
    };
    checkToken();
  }, []);

  return (
    <>
      {!isLoggedIn && <LoginForm setIsLoggedIn={setIsLoggedIn} navigation={navigation} />}
      {isLoggedIn && <UserProfile setIsLoggedIn={setIsLoggedIn} />}
    </>
  );
};

User.navigationOptions = {
  headerTitle: 'Authenticate',
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
  screenCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  authContainer: {
    width: '80%',
    maxWidth: 400,
    maxHeight: 400,
    padding: 20,
  },
  buttonContainer: {
    marginTop: 10,
  },
  input: {
    paddingHorizontal: 2,
    paddingVertical: 5,
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
  },
  label: {
    fontFamily: 'open-sans-bold',
    marginVertical: 8,
  },
});

export default User;
