import React from 'react';
import {StyleSheet, Text, View, Image, ScrollView} from 'react-native';
import {WebView} from 'react-native-webview';
import axios from 'react-native-axios';

export default function Product(props) {
  let product = props.route.params.product;
  const linkImg = `${axios.defaults.baseUrl}/images/${product.image}`

  return (
    // <ScrollView style={styles.container} flexDirection="column">
    <ScrollView style={{flex: 1}}>
      <Image style={styles.img}
        source={{uri: linkImg}}
      />
      <Text style={[styles.text, styles.name]}>{product.name}</Text>
      <Text style={[styles.text, styles.brand]}>{product.brand}</Text>
      <Text style={[styles.text, styles.price]}>{product.price} $</Text>
      {/* <Text style={[styles.text, styles.description]}>{product.description}</Text> */}
      <WebView style={{height: `1000`}}
        originWhitelist={['*']}
        source={{html: product.description}}
        scalesPageToFit={false}
      />
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    padding: 20
  },
  text: {
    color: 'black',
  },
  img: {
    width: "100%",
    aspectRatio: 1,
    alignItems: "center",
  },
  name: {
    fontWeight: "bold",
    fontSize: 36,
  },
  brand: {
    fontWeight: "bold",
    fontSize: 26,
  },
  price: {
    color: "red"
  }
});
