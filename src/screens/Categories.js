import React from "react"
import { StyleSheet, View, FlatList } from "react-native";
import CategoriesItem from "../components/CategoriesItem";
import { useState } from "react";

export default function Categories({navigation}) {
  const [categories, setCategories] = useState([
    { id: 1, name: "Laptop" },
    { id: 2, name: "Mouse" },
    { id: 3, name: "Keyboard" },
    { id: 4, name: "Headphone" },
    { id: 5, name: "Router" },
    { id: 6, name: "Phone" },
  ]);

  return (
    <View style={styles.container}>
        <FlatList
          data={categories}
          renderItem={({ item }) => 
            <CategoriesItem category={item} 
              onPress={() => navigation.navigate("Category", {
                idCategory: item.id,
                nameCategory: item.name,
              })}
            />
          }
          keyExtractor={item => item.id}
        />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center",

    paddingLeft: 16,
    paddingRight: 16,
  },
});