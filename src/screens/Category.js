import React from "react"
import { StyleSheet, View, Text } from "react-native"
import Products from "../components/Products"
import { useState, useEffect } from "react"
import axios from "react-native-axios";

export default function Category(props) {
  let idCategory = props.route.params.idCategory
  let nameCategory = props.route.params.nameCategory
  // const [products, setProducts] = useState([
  //   { id: 1, name: "Laptop1", image: "cs1.png", brand: "APPLE", idCategory: 1, price: 9, description: "", rating: 5},
  //   { id: 2, name: "Laptop2", image: "cs1.png", brand: "APPLE", idCategory: 1, price: 9, description: "", rating: 5},
  //   { id: 3, name: "Laptop3", image: "cs1.png", brand: "APPLE", idCategory: 2, price: 9, description: "", rating: 5},
  //   { id: 4, name: "Laptop4", image: "cs1.png", brand: "APPLE", idCategory: 2, price: 9, description: "", rating: 5},
  //   { id: 5, name: "Laptop5", image: "cs1.png", brand: "APPLE", idCategory: 3, price: 9, description: "", rating: 5},
  //   { id: 6, name: "Laptop6", image: "cs1.png", brand: "APPLE", idCategory: 3, price: 9, description: "", rating: 5},
  //   { id: 7, name: "Laptop7", image: "cs1.png", brand: "APPLE", idCategory: 4, price: 9, description: "", rating: 5},
  //   { id: 8, name: "Laptop8", image: "cs1.png", brand: "APPLE", idCategory: 4, price: 9, description: "", rating: 5},
  //   { id: 9, name: "Laptop9", image: "cs1.png", brand: "APPLE", idCategory: 5, price: 9, description: "", rating: 5},
  //   { id: 10, name: "Laptop10", image: "cs1.png", brand: "DELL", idCategory: 5, price: 9, description: "", rating: 5},
  //   { id: 11, name: "Laptop11", image: "cs1.png", brand: "DELL", idCategory: 6, price: 9, description: "", rating: 5},
  //   { id: 12, name: "Laptop12", image: "cs1.png", brand: "DELL", idCategory: 6, price: 9, description: "", rating: 5},
  //   { id: 13, name: "Laptop13", image: "cs1.png", brand: "DELL", idCategory: 6, price: 9, description: "", rating: 5},
  // ]);
  const path = ""
  const [products, setProducts] = useState([])
  useEffect(() => {
    function fetchOne(x) {
      axios
        .get(`${axios.defaults.baseUrl}/products/`)
        .then((res) => {
          setProducts(res.data)
        })
        .catch((err) => console.error(err))
    }
    fetchOne(path)
  }, [path])

  return (
    <View style={styles.container}>
      <Text style={styles.name}>{nameCategory}</Text>
      <Products products={
        products.filter(product => product.idCategory === idCategory)}/>
    </View>
  );
}

const styles = StyleSheet.create({
  name: {
    fontWeight: "bold",
    padding: 10,
    paddingBottom: 0,
    fontSize: 30,
    textAlign: "center",
  },
});
