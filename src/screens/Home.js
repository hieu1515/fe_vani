import React from "react"
import { View, Text, StyleSheet, TextInput } from "react-native"
import { Picker } from '@react-native-picker/picker'
import { useState, useEffect } from "react"
import Products from "../components/Products"
import axios from "react-native-axios";

const Home = () => {
  const [searchText, setSearchText] = useState("")

  // const [products, setProducts] = useState([
  //   { id: 1, name: "Laptop1", image: "cs1.png", brand: "APPLE", idCategory: 1, price: 9, description: "", rating: 5},
  //   { id: 2, name: "Laptop2", image: "cs1.png", brand: "APPLE", idCategory: 1, price: 9, description: "", rating: 5},
  //   { id: 3, name: "Laptop3", image: "cs1.png", brand: "APPLE", idCategory: 2, price: 9, description: "", rating: 5},
  //   { id: 4, name: "Laptop4", image: "cs1.png", brand: "APPLE", idCategory: 2, price: 9, description: "", rating: 5},
  //   { id: 5, name: "Laptop5", image: "cs1.png", brand: "APPLE", idCategory: 3, price: 9, description: "", rating: 5},
  //   { id: 6, name: "Laptop6", image: "cs1.png", brand: "APPLE", idCategory: 3, price: 9, description: "", rating: 5},
  //   { id: 7, name: "Laptop7", image: "cs1.png", brand: "APPLE", idCategory: 4, price: 9, description: "", rating: 5},
  //   { id: 8, name: "Laptop8", image: "cs1.png", brand: "APPLE", idCategory: 4, price: 9, description: "", rating: 5},
  //   { id: 9, name: "Laptop9", image: "cs1.png", brand: "APPLE", idCategory: 5, price: 9, description: "", rating: 5},
  //   { id: 10, name: "Laptop10", image: "cs1.png", brand: "DELL", idCategory: 5, price: 9, description: "", rating: 5},
  //   { id: 11, name: "Laptop11", image: "cs1.png", brand: "DELL", idCategory: 6, price: 9, description: "", rating: 5},
  //   { id: 12, name: "Laptop12", image: "cs1.png", brand: "DELL", idCategory: 6, price: 9, description: "", rating: 5},
  //   { id: 13, name: "Laptop13", image: "cs1.png", brand: "DELL", idCategory: 6, price: 9, description: "", rating: 5},
  // ]);

  const path = ""
  const [products, setProducts] = useState([])
  useEffect(() => {
    function fetchOne(x) {
      axios
        .get(`${axios.defaults.baseUrl}/products/`)
        .then((res) => {
          setProducts(res.data)
        })
        .catch((err) => console.error(err))
    }
    fetchOne(path)
  }, [path])

  const [brands, setBrands] = useState([
    { id: 1, name: "HP" },
    { id: 2, name: "TP-Link" },
    { id: 3, name: "NETGEAR" },
    { id: 4, name: "Motorola" },
    { id: 5, name: "TCL" },
    { id: 6, name: "TECKNET" },
    { id: 7, name: "Trueque" },
    { id: 8, name: "Samsung" },
  ])

  const [selectedValue, setSelectedValue] = useState(0)

  function onSearchTextChange(text) {
    setSearchText(text)
  }

  return (
    <View style={styles.container}>
      <Text style={[styles.text, styles.banner]}>Computer Shop</Text>
      <TextInput
        style={styles.input}
        onChangeText={onSearchTextChange}
        value={searchText}
        placeholder="Search"
        placeholderTextColor="grey"
      />

      <View style={{ paddingLeft: 12 }}>
        <Picker
          selectedValue={selectedValue}
          style={[styles.text, styles.picker]}
          onValueChange={(itemValue, itemIndex) => {
            setSelectedValue(itemValue)
          }}
          >
          <Picker.Item label="All" value={0} key={0} />
          { brands.map((brand) => (
            <Picker.Item label={brand.name} value={brand.id} key={brand.id}/>
            ))}
        </Picker>
      </View>

      <Products
        products={products.filter((item) =>
          selectedValue === 0
            ? item.name.toLocaleLowerCase().includes(searchText.toLocaleLowerCase())
            : (item.name.toLocaleLowerCase().includes(searchText.toLocaleLowerCase()) &&
              item.brand === brands[selectedValue - 1].name)
        )}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  text: {
    color: "black"
  },
  banner: {
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 36,
    color: "red"
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    borderColor: 'grey',
  },
  picker: {
    height: 50, 
    width: 150, 
    backgroundColor: '#c7dbff',
  },
})

export default Home
