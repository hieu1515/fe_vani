import React from 'react';
import { StyleSheet, Text } from "react-native";

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Categories from "./src/screens/Categories";
import Category from "./src/screens/Category";
import Home from "./src/screens/Home";
import Notification from "./src/screens/Notification";
import Cart from "./src/screens/Cart";
import User from "./src/screens/User";

import Icon from 'react-native-vector-icons/Ionicons';
import Product from './src/screens/Product';
import SignUp from "./src/components/SignUp";
import axios from "react-native-axios";
import AsyncStorage from '@react-native-async-storage/async-storage';


const Stack = createNativeStackNavigator()
export default function App() {
  axios.defaults.baseUrl = "http://api.uniproject.xyz/eshop"
  const setDefaultToken = async () => {
    const token = await AsyncStorage.getItem('token')
    if (token) axios.defaults.headers['Authorization'] = `Bearer ${token}`;
  }
  setDefaultToken();



  return (
    <NavigationContainer style={styles.container}>
      <Text style={styles.header}></Text>

      <Stack.Navigator screenOptions={{headerShown:false}}>      
        <Stack.Screen
          name="TabMe"
          component={TabMe}
        />
        <Stack.Screen name="Category" component={Category} />
        <Stack.Screen name="Product" component={Product} />
        <Stack.Screen name="SignUp" component={SignUp} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const Tab = createBottomTabNavigator()
function TabMe() {
  return (
    <Tab.Navigator
      initialRouteName="User"
      barStyle={{ backgroundColor: 'ghostwhite' }}
      
      screenOptions={ ({ route }) => ({
        tabBarIcon: ({ focused, color="orange", size=24 }) => {
          let iconName
          // console.log("aaaa")
          if (route.name === 'Home') {
            iconName = focused ? 'home': 'home-outline'
          } else if (route.name === 'Categories') {
            iconName = focused ? 'list': 'list-outline'
          } else if (route.name === 'Notification') {
            iconName = focused ? 'notifications': 'notifications-outline'
          } else if (route.name === 'Cart') {
            iconName = focused ? 'cart': 'cart-outline'
          } else if (route.name === 'User') {
            iconName = focused ? 'settings': 'settings-outline'
          }

          return <Icon name={iconName} size={size} color={color} />
        },
        tabBarActiveTintColor: 'tomato',
        tabBarInactiveTintColor: 'gray',
        headerShown: false,
      })}
    >
      <Tab.Screen name='Home' component={Home} 
        // options={{ tabBarIcon: 
        //   (isFocused) => (<Icon name={isFocused ? "home" : 'home-outline'} />)
        // }}
      />
      <Tab.Screen name='Categories' component={Categories}/>
      <Tab.Screen name='Notification' component={Notification}
        options={{ tabBarBadge: 0 }}
      />
      <Tab.Screen name='Cart' component={Cart}
        options={{ tabBarBadge: 0 }}
      />
      <Tab.Screen name='User' component={User}/>
    </Tab.Navigator>
  )
};


const styles = StyleSheet.create({
  header: {
    marginTop: 8,
  }
});